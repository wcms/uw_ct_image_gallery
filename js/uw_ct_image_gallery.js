/**
 * @file
 * Owl 1.x documentation: http://web.archive.org/web/20160306020452/http://www.owlgraphic.com/owlcarousel/
 */

(function ($) {

  Drupal.behaviors.uw_ct_image_gallery = {

    attach: function (context, settings) {

      if ($('[id*="image-gallery-carousel"]').length) {

        $('[id*="image-gallery-carousel"]').each(function (index) {

          $('.content').addClass('uw-image-gallery');

          // Set variable current_ig to the id of the current image gallery that is in the loop.
          var current_ig = '#' + $(this).attr('id');
          var nid = current_ig.replace('#image-gallery-carousel', '');

          // Select all of the images in the gallery.
          $(current_ig).find('img').each(function (index2) {
            $(this).parent().parent().attr('data-ig-index', index2);
            $(this).parent().parent().attr('data-ig-type', 'carousel');
            $(this).parent().parent().attr('data-nid', nid);
          });

          $(current_ig).find('.image-caption-wrapper').each(function (index3) {
            $(this).attr('data-ig-index', index3);
          });

          $carousel_items = parseInt($(this).attr('data-carousel-items'));

          // get options and convert to a JS object
          $carousel_options = $(this).attr('data-carousel-options');
          $custom_options = $.parseJSON($carousel_options);
          // If there is more than 1 slide displayed, add responsive options
          if ($carousel_items > 1) {
            $carousel_items > 3 ? $desktop_items = $carousel_items -1 : $desktop_items = $carousel_items;
            $responsive_options = {
              itemsDesktop: [2000, $carousel_items],
              itemsDesktopSmall: [1200, $desktop_items],
              itemsTablet: [1000,2],
              itemsMobile: [600, 1]
            }
            $.extend($custom_options, $responsive_options);
            $(this).addClass('multiple-items');
          }

          // Set the default options
          $options = {
            slideSpeed: 300,
            items: $carousel_items,
            itemsScaleUp: true,
            lazyLoad: true,
            autoHeight: false,
            autoWidth: true,
            rewindSpeed: 10
          };

          // Merge the custom options into the default options
          $.extend($options, $custom_options);

          // Create the carousel
          $(this).owlCarousel($options);
        });
      }

      // Setup embedded image gallery when using thumbnails.
      if ($('[id*="image-gallery-thumbnail"]').length) {

        $('[id*="image-gallery-thumbnail"]').each(function (index) {

          // Add the class to distinguish between other embeds.
          $('.content_node').addClass('uw-image-gallery');

          // Set variable current_ig to the id of the current image gallery that is in the loop.
          var current_ig = '#' + $(this).attr('id');
          var nid = current_ig.replace('#image-gallery-thumbnail', '');

          // Select all of the images in the gallery.
          $(current_ig).find('img').each(function (index2) {
            $(this).parent().attr('data-ig-index', index2);
            $(this).parent().attr('data-ig-type', 'thumbnail');
            $(this).parent().attr('data-nid', nid);
          });

          // Add extra div for the image without caption.
          $(current_ig).find('.item').each(function () {
            if ($(this).find('.image-caption-wrapper.element-invisible').length == 0) {
              var image_caption = '<div class="image-caption-wrapper element-invisible"></div>';
                $(this).append(image_caption);
              }
          });

          $(current_ig).find('.image-caption-wrapper').each(function (index3) {
            $(this).attr('data-ig-index', index3);
          });

        });
      }

      // This is the onComplete function for colorbox.
      // Adding the caption to the colorbox.
      $(context).bind('cbox_complete', function () {
        // Get the current class of image gallery from the a tag data elements.
        var ig_index = $.colorbox.element().attr('data-ig-index');
        var ig_type = $.colorbox.element().attr('data-ig-type');
        var nid = $.colorbox.element().attr('data-nid');

        // Find the current index of the image by using the cboxCurrent of colorbox.
        // cboxCurrent will have 1 of 6, 2 of 6, etc ..
        current_index = $(this).find('#cboxCurrent').html();
        current_index = current_index.substr(0,current_index.indexOf(' ')) - 1;
        current_title = $(this).find('#cboxTitle');

        // Setup the image gallery id.
        if (ig_type == "carousel") {
          var ig_caption_id = '#image-gallery-carousel' + nid;
        }
        if (ig_type == "thumbnail") {
          var ig_caption_id = '#image-gallery-thumbnail' + nid;
        }

        // Get the current caption based on the nid.
        current_caption = $(ig_caption_id).find(".image-caption-wrapper[data-ig-index='" + current_index + "']").find('p');

        // Set the current caption to the title of the colourbox.
        $(current_title).html(current_caption.html());

        // Ensure that if there is a caption to show it.
        if ($(current_title).html() !== "") {
          $(current_title).show();
        }

      });

    }
  };
})(jQuery);
