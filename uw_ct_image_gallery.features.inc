<?php

/**
 * @file
 * uw_ct_image_gallery.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_image_gallery_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_image_gallery_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function uw_ct_image_gallery_image_default_styles() {
  $styles = array();

  // Exported image style: image_gallery_small.
  $styles['image_gallery_small'] = array(
    'label' => 'image_gallery_small',
    'effects' => array(
      3 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 320,
          'height' => 380,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: image_gallery_squares.
  $styles['image_gallery_squares'] = array(
    'label' => 'image_gallery_squares',
    'effects' => array(
      4 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: image_gallery_standard.
  $styles['image_gallery_standard'] = array(
    'label' => 'image_gallery_standard',
    'effects' => array(
      1 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 496,
          'height' => 330,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: image_gallery_wide.
  $styles['image_gallery_wide'] = array(
    'label' => 'image_gallery_wide',
    'effects' => array(
      2 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 752,
          'height' => 500,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_ct_image_gallery_node_info() {
  $items = array(
    'uw_image_gallery' => array(
      'name' => t('Image gallery'),
      'base' => 'node_content',
      'description' => t('A javascript based image gallery.  Includes body text and images.'),
      'has_title' => '1',
      'title_label' => t('Gallery name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
