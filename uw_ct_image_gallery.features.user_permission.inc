<?php

/**
 * @file
 * uw_ct_image_gallery.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_image_gallery_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_image_gallery content'.
  $permissions['create uw_image_gallery content'] = array(
    'name' => 'create uw_image_gallery content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_image_gallery content'.
  $permissions['delete any uw_image_gallery content'] = array(
    'name' => 'delete any uw_image_gallery content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_image_gallery content'.
  $permissions['delete own uw_image_gallery content'] = array(
    'name' => 'delete own uw_image_gallery content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_image_gallery content'.
  $permissions['edit any uw_image_gallery content'] = array(
    'name' => 'edit any uw_image_gallery content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_image_gallery content'.
  $permissions['edit own uw_image_gallery content'] = array(
    'name' => 'edit own uw_image_gallery content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_image_gallery revision log entry'.
  $permissions['enter uw_image_gallery revision log entry'] = array(
    'name' => 'enter uw_image_gallery revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_image_gallery authored by option'.
  $permissions['override uw_image_gallery authored by option'] = array(
    'name' => 'override uw_image_gallery authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_image_gallery authored on option'.
  $permissions['override uw_image_gallery authored on option'] = array(
    'name' => 'override uw_image_gallery authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_image_gallery promote to front page option'.
  $permissions['override uw_image_gallery promote to front page option'] = array(
    'name' => 'override uw_image_gallery promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_image_gallery published option'.
  $permissions['override uw_image_gallery published option'] = array(
    'name' => 'override uw_image_gallery published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_image_gallery revision option'.
  $permissions['override uw_image_gallery revision option'] = array(
    'name' => 'override uw_image_gallery revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_image_gallery sticky option'.
  $permissions['override uw_image_gallery sticky option'] = array(
    'name' => 'override uw_image_gallery sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
