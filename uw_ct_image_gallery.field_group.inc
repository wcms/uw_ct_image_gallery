<?php

/**
 * @file
 * uw_ct_image_gallery.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_image_gallery_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image_gallery_settings|node|uw_image_gallery|form';
  $field_group->group_name = 'group_image_gallery_settings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_image_gallery';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Slideshow Settings',
    'weight' => '1',
    'children' => array(
      0 => 'field_image_gallery_type',
      1 => 'field_image_gallery_arrows',
      2 => 'field_image_gallery_num',
      3 => 'field_image_gallery_pager',
      4 => 'field_image_gallery_nav_options',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-image-gallery-settings field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_image_gallery_settings|node|uw_image_gallery|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Slideshow Settings');

  return $field_groups;
}
