/**
 * @file
 * Define tags as "block level" so the editor doesn't try to put paragraph tags around them.
 */

cktags = ['ckimagegallery'];

// Global variable used to store fakeImage element when socialmedia is double clicked.
var doubleclick_element;

CKEDITOR.plugins.add('galleryembedded', {
  requires : ['dialog', 'fakeobjects'],
  init: function (editor) {
    // Define plugin name.
    var pluginName = 'galleryembedded';

    // Set up object to share variables amongst scripts.
    CKEDITOR.galleryembedded = {};

    // Register button for Imagegallery.
    editor.ui.addButton('Imagegallery', {
      label : "Add/Edit Image gallery",
      command : 'imagegallery',
      icon: this.path + 'icons/imagegallery.png',
    });
    // Register right-click menu item for Imagegallery.
    editor.addMenuItems({
      imagegallery : {
        label : "Edit Image gallery",
        icon: this.path + 'icons/imagegallery.png',
        command : 'imagegallery',
        group : 'image',
        order : 1
      }
    });
    // Make ckimagegallery to be a self-closing tag.
    CKEDITOR.dtd.$empty['ckimagegallery'] = 1;
    // Make sure the fake element for Imagegallery has a name.
    CKEDITOR.galleryembedded.ckimagegallery = 'Imagegallery';
    CKEDITOR.lang.en.fakeobjects.ckimagegallery = CKEDITOR.galleryembedded.ckimagegallery;
    // Add JavaScript file that defines the dialog box for Imagegallery.
    CKEDITOR.dialog.add('imagegallery', this.path + 'dialogs/imagegallery.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('imagegallery', new CKEDITOR.dialogCommand('imagegallery'));

    // Regular expressions for imagegallery.
    CKEDITOR.galleryembedded.imagegallery_regex = /^[1-9][0-9]*$/;

    // Open the appropriate dialog box if an element is double-clicked.
    editor.on('doubleclick', function (evt) {
      var element = evt.data.element;

      // Store the element as global variable to be used in onShow of dialog, when double clicked.
      doubleclick_element = element;

      if (element.is('img') && element.data('cke-real-element-type') === 'ckimagegallery') {
        evt.data.dialog = 'imagegallery';
      }
    });

    // Add the appropriate right-click menu item if an element is right-clicked.
    if (editor.contextMenu) {
      editor.contextMenu.addListener(function (element, selection) {
        if (element && element.is('img') && element.data('cke-real-element-type') === 'ckimagegallery') {
          return { imagegallery : CKEDITOR.TRISTATE_OFF };
        }
      });
    }

    // Add CSS to use in-editor to style custom (fake) elements.
    CKEDITOR.addCss(
      'img.ckimagegallery {\
        background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/imagegallery.png') + ');\
        background-position: center center;\
        background-repeat: no-repeat;\
        background-color: #000;\
        width: 100%;\
        height: 50px;\
        margin: 0 0 10px 0;\
        margin-left: auto;\
        margin-right: auto;\
      }' +

      // Imagegallery: Definitions for wide width.
      '.uw_tf_standard_wide p img.ckimagegallery {\
        height: 50px;\
      }' +

      // Imagegallery: Definitions for standard width.
      '.uw_tf_standard p img.ckimagegallery {\
        height: 50px;\
      }'
    );
  },
  afterInit : function (editor) {
    // Make fake image display on first load/return from source view.
    if (editor.dataProcessor.dataFilter) {
      editor.dataProcessor.dataFilter.addRules({
        elements : {
          ckimagegallery : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.ckimagegallery = CKEDITOR.galleryembedded.ckimagegallery;
            // Adjust title if a list name is present.
            if (element.attributes['data-imagegallerynid']) {
              CKEDITOR.lang.en.fakeobjects.ckimagegallery += ': ' + element.attributes['data-imagegallerynid'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'ckimagegallery', 'ckimagegallery', false);
          }
        }
      });
    }
  }
});
