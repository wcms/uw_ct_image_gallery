/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var imagegalleryDialog = function (editor) {
    return {
      title : 'Image gallery Properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'imagegallery',
        label: 'imagegallery',
        elements:[
          {
            type: 'text',
            id: 'imagegalleryInput',
            label: 'Please enter image gallery node ID:',
            required: true,
            setup: function (element) {
              this.setValue(element.getAttribute('data-imagegallerynid'));
            }
        },
          {
            type: 'select',
            id: 'gallerytype',
            label: 'Which type of image gallery is to be embedded:',
            items: [ [ 'Thumbnails' ], [ 'Carousel' ]], 'default': 'Thumbnails',
            required: true,
            setup: function (element) {
              this.setValue(element.getAttribute('data-gallerytype'));
            }
        }
        ]
      }],
      onOk: function () {
        // Get form information.
        imagegalleryInput = this.getValueOf('imagegallery','imagegalleryInput');
        gallerytype = this.getValueOf('imagegallery','gallerytype');

        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = "";

        if (!CKEDITOR.galleryembedded.imagegallery_regex.test(imagegalleryInput)) {
            errors += "You must enter a node ID.\r\n";
        }
        else {
          errors = '';
        }
        if (!imagegalleryInput) {
          errors = "You must enter a node ID.\r\n";
        }

        if (errors == '') {
          // Variable used to store number of image gallery (call to php function).
          var num_of_image;

          // Use jQuery ajax call to check if nid supplied is an image gallery.
          jQuery(function ($) {

            // Make the ajax call, set to synchronous so that we wait to get a response.
            $.ajax({
              type: 'get',
              async: false,
              url: Drupal.settings.basePath + "ajax/nid_exists/" + imagegalleryInput + "/uw_image_gallery",

              // If we have success set to the global variable, so that we can use it later to check for errors.
              success: function (data) {
                num_of_image = data;
              }
            });
          });

          // If the number of image gallery is 0 (meaning there are no image gallery node with that nid), set the errors.
          if (num_of_image == 0) {
            errors = 'You must enter a valid image gallery node ID.\r\n';
          }
        }

        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the imagegallery element.
          var ckimagegalleryNode = new CKEDITOR.dom.element('ckimagegallery');
          // Save contents of dialog as attributes of the element.
          ckimagegalleryNode.setAttribute('data-imagegallerynid', imagegalleryInput);
          ckimagegalleryNode.setAttribute('data-gallerytype', gallerytype);
          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.ckimagegallery = CKEDITOR.socialmedia.ckimagegallery + ': ' + imagegalleryInput;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
          var newFakeImage = editor.createFakeElement(ckimagegalleryNode, 'ckimagegallery', 'ckimagegallery', false);
          // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
          newFakeImage.addClass('ckimagegallery');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckimagegallery = CKEDITOR.socialmedia.ckimagegallery;
        }
      },
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.ckimagegalleryNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = fakeImage;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckimagegallery') {
          this.fakeImage = fakeImage;
          var ckimagegalleryNode = editor.restoreRealElement(fakeImage);
          this.ckimagegalleryNode = ckimagegalleryNode;
          this.setupContent(ckimagegalleryNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('imagegallery', function (editor) {
    return imagegalleryDialog(editor);
  });
})();
