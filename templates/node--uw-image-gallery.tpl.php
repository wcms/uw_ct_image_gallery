<?php

/**
 * @file
 * Image gallery template.
 */

if (arg(0) == 'node' && is_numeric(arg(1))) {
  // Get the nid.
  $nid = arg(1);
  $node = node_load($nid);
}

if (isset($content['body'])) {
  print render($content['body']);
}

$carousel = isset($content['field_image_gallery_type']['#items'][0]['value']) && $content['field_image_gallery_type']['#items'][0]['value'] == 1;

if (isset($content['field_uw_image_gallery_images'])) { ?>
  <div id="image-gallery-<?php
  print $carousel ? 'carousel' : 'thumbnail';
  print $nid;
  ?>"
  data-carousel-items="<?php print $carousel_num_items; ?>"
  data-carousel-options='{<?php print $carousel_options; ?>}'
  >
    <?php foreach($content['field_uw_image_gallery_images'] as $key => $image) { ?>
      <?php if (is_int($key)) { ?>
        <div class="item">
          <div class="image-gallery-wrapper">
            <?php
              $file = $image['#item']['uri'];
              $alt_text = check_plain($image['#item']['alt']);
              // Strip any HTML from the caption and clean it up to plain text
              // as much as possible. The inner strip_tags() removes any HTML.
              // The html_entity_decode() converts HTML entities to "normal"
              // characters. The outer strip_tags() removes any HTML that might
              // have been created from &lt;/&gt; in the original source. Needed
              // because the pop-up caption will try to render it, and it might
              // be <script>. The check_plain is Drupal's "official" cleanup.
              if (isset($image['#item']['image_field_caption']['value']) && $image['#item']['image_field_caption']['value'] !== "") {
                $clean_caption = check_plain(strip_tags(html_entity_decode(strip_tags($image['#item']['image_field_caption']['value']), ENT_QUOTES)));
                $caption = 'title="' . $clean_caption . '"';
              }
              else {
                $caption = '';
              }
              preg_match_all('/<a([^>]*?)>/s', render($image), $link);
              print preg_replace('/title=\"([^"]*?)\"/s', $caption, $link[0][0]);

              if ($carousel) {
                if ($node->uw_page_settings_node_enable) {
                  $image_desktop = image_style_url('image_gallery_wide', $file);
                }
                else {
                  $image_desktop = image_style_url('image_gallery_standard', $file);
                }
                $image_small = image_style_url('image_gallery_small', $file);
              ?>
                <picture>
                  <source srcset="<?php print $image_desktop; ?>" media="(min-width: 769px)">
                  <source srcset="<?php print $image_small; ?>" media="(min-width: 320px)">
                  <source srcset="<?php print $image_desktop; ?>">
                  <img src="<?php print $image_desktop; ?>" alt="<?php print $alt_text; ?>">
                </picture>
                <?php if (isset($image['#item']['title'])) { ?>
                  <?php if ($image['#item']['title'] !== "") { ?>
                    <div class="image-title"><?php print check_plain($image['#item']['title']); ?></div>
                  <?php } ?>
                <?php } ?>
              <?php }
              else { ?>
                <img src="<?php print image_style_url('image_gallery_squares', $file); ?>" />
                <?php if (isset($image['#item']['title'])) { ?>
                  <?php if ($image['#item']['title'] !== "") { ?>
                    <div class="image-title element-invisible"><?php print check_plain($image['#item']['title']); ?></div>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
            </a>
          </div>
          <div class="image-caption-wrapper<?php print $carousel ? '' : ' element-invisible'; ?>">
            <?php if (isset($image['#item']['image_field_caption']['value']) && $image['#item']['image_field_caption']['value'] !== "") { ?>
              <div class="image-caption">
                <p class="caption"><?php print $clean_caption; ?></p>
              </div>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
    <?php } ?>
  </div>
<?php } ?>

<?php
if (isset($content['field_image_gallery_bottom'])) {
  print render($content['field_image_gallery_bottom']);
}
